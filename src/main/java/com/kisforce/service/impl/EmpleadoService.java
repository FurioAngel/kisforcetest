package com.kisforce.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kisforce.model.Empleado;
import com.kisforce.service.IEmpleadoService;

@Service
public class EmpleadoService implements IEmpleadoService {

	@Override
	public List<Empleado> getEmpleados() {

		HSSFWorkbook wb = null;
		HSSFSheet sheet = null;
		List<Empleado> lstEmpleados = new ArrayList<Empleado>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		final String DATASOURCE = "empleadosBD.xls";

		try {

			wb = new HSSFWorkbook(new ClassPathResource(DATASOURCE).getInputStream());
			sheet = wb.getSheetAt(0);

			sheet.forEach(row -> {

				lstEmpleados.add(new Empleado(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(),
						row.getCell(2).getStringCellValue(), row.getCell(3).getStringCellValue(),
						row.getCell(4).getStringCellValue(), row.getCell(5).getStringCellValue()));

			});
			
			lstEmpleados.remove(0);

			jsonString = mapper.writeValueAsString(lstEmpleados);
			System.out.println(jsonString);

		} catch (IOException e) {
			System.out.println("Ocurrio un error del tipo: " + e.getMessage());
		}

		return lstEmpleados;
	}

	@Override
	public ByteArrayInputStream getExcel(List<Empleado> lstEmpleados) {

		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ByteArrayInputStream input = null;

		try {

			Sheet sheet = workbook.createSheet("Empleados");

			Row row = sheet.createRow(0);
			CellStyle headerCellStyle = workbook.createCellStyle();

			Cell cell = row.createCell(0);
			cell.setCellValue("Nombre");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell(1);
			cell.setCellValue("Cargo");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell(2);
			cell.setCellValue("Supervisor");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell(3);
			cell.setCellValue("Clase");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell(4);
			cell.setCellValue("Subsidiaria");
			cell.setCellStyle(headerCellStyle);

			cell = row.createCell(5);
			cell.setCellValue("Departamento");
			cell.setCellStyle(headerCellStyle);

			for (int i = 0; i < lstEmpleados.size(); i++) {
				Row dataRow = sheet.createRow(i + 1);
				dataRow.createCell(0).setCellValue(lstEmpleados.get(i).getNombre());
				dataRow.createCell(1).setCellValue(lstEmpleados.get(i).getCargo());
				dataRow.createCell(2).setCellValue(lstEmpleados.get(i).getSupervisor());
				dataRow.createCell(3).setCellValue(lstEmpleados.get(i).getClase());
				dataRow.createCell(4).setCellValue(lstEmpleados.get(i).getSubsidiaria());
				dataRow.createCell(5).setCellValue(lstEmpleados.get(i).getDepartamento());
			}

			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
			sheet.autoSizeColumn(5);
			sheet.autoSizeColumn(6);

			workbook.write(outputStream);
			input = new ByteArrayInputStream(outputStream.toByteArray());

		} catch (IOException e) {
			System.out.println("Ocurrio un IOException " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Ocurrio un IOException " + e.getMessage());
		}

		return input;

	}

	@Override
	public void getCsv(PrintWriter writer, List<Empleado> lstEmpleados) {

		try {

			writer.write(
					"Nombre  		     Cargo  		       Supervisor   		        Clase  			          Subsidiaria               Departamento \n");

			lstEmpleados.forEach(e -> writer
					.write(e.getNombre() + "," + e.getCargo() + "," + e.getSupervisor() + "," + e.getSupervisor() + ","
							+ e.getClase() + "," + e.getSubsidiaria() + "," + e.getDepartamento() + "\n"));

		} catch (Exception e) {
			System.out.println("Error en el archivo CSV" + e.getMessage());
		}

	}

	@Override
	public ByteArrayInputStream getPdf(List<Empleado> lstEmpleados) {

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {

			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(90);

			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Nombre", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Cargo", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Supervisor", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Clase", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Subsidiaria", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Departamento", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			lstEmpleados.forEach(e -> {

				PdfPCell cell;

				cell = new PdfPCell(new Phrase(e.getNombre()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(e.getCargo()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(e.getSupervisor()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(e.getClase()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(e.getSubsidiaria()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(e.getDepartamento()));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

			});

			PdfWriter.getInstance(document, out);
			document.open();
			document.add(table);

			document.close();

		} catch (DocumentException e) {
			System.out.println("Ocurrio un error en el PDF " + e.getMessage());
		}

		return new ByteArrayInputStream(out.toByteArray());
	}
}
