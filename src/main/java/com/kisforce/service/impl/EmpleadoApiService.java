package com.kisforce.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.kisforce.model.Empleado;
import com.kisforce.service.IEmpleadoApiService;


@Service
public class EmpleadoApiService implements IEmpleadoApiService {

	@Override
	public List<Empleado> getEmpleados() {

		HSSFWorkbook wb = null;
		HSSFSheet sheet = null;
		List<Empleado> lstEmpleados = new ArrayList<Empleado>();

		try {

			wb = new HSSFWorkbook(new ClassPathResource("empleadosBD.xls").getInputStream());
			sheet = wb.getSheetAt(0);

			sheet.forEach(row -> {

				lstEmpleados.add(new Empleado(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(),
						row.getCell(2).getStringCellValue(), row.getCell(3).getStringCellValue(),
						row.getCell(4).getStringCellValue(), row.getCell(5).getStringCellValue()));

			});

			lstEmpleados.remove(0);

		} catch (IOException e) {
			System.out.println("Ocurrio un error del tipo: " + e.getMessage());
		}

		return lstEmpleados;
	}

}
