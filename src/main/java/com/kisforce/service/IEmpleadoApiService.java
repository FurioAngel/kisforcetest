package com.kisforce.service;

import java.util.List;

import com.kisforce.model.Empleado;

public interface IEmpleadoApiService {
	
	 List<Empleado> getEmpleados();

}
