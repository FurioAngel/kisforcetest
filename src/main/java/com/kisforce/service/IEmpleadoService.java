package com.kisforce.service;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.util.List;

import com.kisforce.model.Empleado;

public interface IEmpleadoService {

	
	
	List<Empleado> getEmpleados();
	
	
	ByteArrayInputStream getExcel(List<Empleado> lstEmpleados);
	
	
	void getCsv(PrintWriter writer, List<Empleado> lstEmpleados);
	
	
	ByteArrayInputStream getPdf(List<Empleado> lstEmpleados);
	
	
}
