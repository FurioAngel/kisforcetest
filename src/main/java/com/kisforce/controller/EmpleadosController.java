package com.kisforce.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.kisforce.client.EmpleadoApiClient;
import com.kisforce.model.Empleado;
import com.kisforce.service.IEmpleadoService;

@Controller
public class EmpleadosController {

	@Autowired
	private IEmpleadoService empleadosService;

	@Autowired
	EmpleadoApiClient empleadoApiClient;

	@GetMapping("/getEmpleados")
	public String getEmpleados(Model model, HttpServletRequest request) {
		List<Empleado> lstEmpleado = new ArrayList<Empleado>();
		lstEmpleado = empleadoApiClient.getClientesApi();

		request.getSession().setAttribute("lstInicial", lstEmpleado);

		model.addAttribute("empleados", lstEmpleado);
		model.addAttribute("empleadorform", new Empleado());

		return "muestraempleados";
	}

	@PostMapping("/filterEmpleados")
	public String filterEmpleados(@ModelAttribute("empleadorform") Empleado empleadorform, BindingResult bindingResult,
			HttpServletRequest request, Model model) {

		List<Empleado> subLstEmpleado = null;
		List<Empleado> lstEmpleado = (List<Empleado>) request.getSession().getAttribute("lstInicial");

		subLstEmpleado = lstEmpleado.stream().filter(e -> e.equals(empleadorform)).collect(Collectors.toList());

		request.getSession().removeAttribute("lstActual");
		request.getSession().setAttribute("lstActual", subLstEmpleado);

		model.addAttribute("empleados", subLstEmpleado);

		return "muestraempleados";
	}

	@GetMapping("/getExcel")
	public void getExcel(HttpServletResponse response, HttpServletRequest request) {
		List<Empleado> lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstActual");

		if (lstEmpleados == null) {
			lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstInicial");
		}

		try {

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=Empleados.xls");
			ByteArrayInputStream stream = empleadosService.getExcel(lstEmpleados);
			IOUtils.copy(stream, response.getOutputStream());

		} catch (IOException e) {
			System.out.println("Error : " + e.getMessage());
		}

	}

	@GetMapping("/getCsv")
	public void getCsv(HttpServletResponse response, HttpServletRequest request) {

		List<Empleado> lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstActual");

		if (lstEmpleados == null) {
			lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstInicial");
		}

		try {

			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=Empleados.csv");
			System.out.println("tamaño" + lstEmpleados.size());
			empleadosService.getCsv(response.getWriter(), lstEmpleados);

		} catch (IOException e) {
			System.out.println("Error : " + e.getMessage());
		}

	}

	@GetMapping("/getPdf")
	public void getPdf(HttpServletResponse response, HttpServletRequest request) {

		List<Empleado> lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstActual");

		if (lstEmpleados == null) {
			lstEmpleados = (List<Empleado>) request.getSession().getAttribute("lstInicial");
		}

		try {

			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=Empleados.pdf");
			ByteArrayInputStream stream = empleadosService.getPdf(lstEmpleados);
			IOUtils.copy(stream, response.getOutputStream());

		} catch (IOException e) {
			System.out.println("Error : " + e.getMessage());
		}

	}

}