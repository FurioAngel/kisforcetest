package com.kisforce.model;

public class Empleado {

	private String nombre;

	private String cargo;

	private String supervisor;

	private String clase;

	private String subsidiaria;

	private String departamento;

	public Empleado(String nombre, String cargo, String supervisor, String clase, String subsidiaria,
			String departamento) {
		super();
		this.nombre = nombre;
		this.cargo = cargo;
		this.supervisor = supervisor;
		this.clase = clase;
		this.subsidiaria = subsidiaria;
		this.departamento = departamento;
	}

	public Empleado() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getSubsidiaria() {
		return subsidiaria;
	}

	public void setSubsidiaria(String subsidiaria) {
		this.subsidiaria = subsidiaria;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", cargo=" + cargo + ", supervisor=" + supervisor + ", clase=" + clase
				+ ", subsidiaria=" + subsidiaria + ", departamento=" + departamento + "]";
	}

	@Override
	public boolean equals(Object obj) {

		final Empleado empl = (Empleado) obj;

		if (!empl.getSupervisor().equals("")) {
			if (!getSupervisor().equals(empl.getSupervisor()))
				return false;
		}

		if (!empl.getClase().equals("")) {
			if (!getClase().equals(empl.getClase()))
				return false;
		}

		if (!empl.getDepartamento().equals("")) {
			if (!getDepartamento().equals(empl.getDepartamento()))
				return false;
		}

		if (!empl.getSubsidiaria().equals("")) {
			if (!getSubsidiaria().equals(empl.getSubsidiaria()))
				return false;
		}

		return true;

	}

}
