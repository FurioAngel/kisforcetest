package com.kisforce.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kisforce.model.Empleado;
import com.kisforce.service.IEmpleadoApiService;

@RestController
public class EmpleadosRestController {

	@Autowired
	IEmpleadoApiService empleadoApiService;

	@GetMapping("/getEmpleadosApi")
	public ResponseEntity<List<Empleado>> getEmpleadosApi() {

		List<Empleado> lstEmpleado = empleadoApiService.getEmpleados();

		lstEmpleado.forEach(System.out::println);

		return new ResponseEntity<List<Empleado>>(lstEmpleado, HttpStatus.OK);

	}

}
