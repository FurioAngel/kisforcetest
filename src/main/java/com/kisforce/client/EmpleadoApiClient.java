package com.kisforce.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kisforce.model.Empleado;

@Component
public class EmpleadoApiClient {

	@Autowired
	RestTemplate restemplate;

	public List<Empleado> getClientesApi() {

		final String URI = "http://localhost:8080/getEmpleadosApi";
		List<Empleado> lstEmpleados = null;
		ResponseEntity<Empleado[]> response = null;

		try {

			response = restemplate.getForEntity(URI, Empleado[].class);
			lstEmpleados = Arrays.asList(response.getBody());

		} catch (Exception e) {
			// TODO: handle exception
		}
		return lstEmpleados;
	}

}
